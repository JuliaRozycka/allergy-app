package edu.ib.zpo2project.account

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import edu.ib.zpo2project.R

class RegisterActivity : BaseActivity() {

    private var registerButton: Button? = null
    private var inputEmail: TextInputLayout? = null
    private var inputName: TextInputLayout? = null
    private var inputPassword: TextInputLayout? = null
    private var inputRepPass: TextInputLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.hide()

        registerButton = findViewById(R.id.registerbtn)
        inputEmail = findViewById(R.id.registerEmail)
        inputName = findViewById(R.id.registerName)
        inputPassword = findViewById(R.id.registerPassword)
        inputRepPass = findViewById(R.id.confPassword)

        registerButton?.setOnClickListener{
            //validateRegisterDetails()
            registerUser()

        }
    }

    private fun registerUser() {
        if (validateRegisterDetails()){
            val login: String = inputEmail?.editText?.text.toString().trim() {it <= ' '}
            val password: String = inputPassword?.editText?.text.toString().trim() {it <= ' '}

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(login,password).addOnCompleteListener(
                OnCompleteListener <AuthResult>{ task ->
                    if(task.isSuccessful){
                        val firebaseUser: FirebaseUser = task.result!!.user!!
                        showErrorSnackBar("You are registered successfully.",false)
                        FirebaseAuth.getInstance().signOut()
                        finish()


                    } else{
                        showErrorSnackBar(task.exception!!.message.toString(),true)
                    }

                }
            )

        }
    }

    private fun validateRegisterDetails(): Boolean {

        return when{
            TextUtils.isEmpty(inputEmail?.editText?.text.toString().trim{ it <= ' '}) -> {
                showErrorSnackBar(resources.getString(R.string.err_msg_enter_email),true)
                false
            }
            TextUtils.isEmpty(inputName?.editText?.text.toString().trim{ it <= ' '}) -> {
                showErrorSnackBar(resources.getString(R.string.err_msg_enter_name),true)
                false
            }

            TextUtils.isEmpty(inputPassword?.editText?.text.toString().trim{ it <= ' '}) -> {
                showErrorSnackBar(resources.getString(R.string.err_msg_enter_password),true)
                false
            }

            TextUtils.isEmpty(inputRepPass?.editText?.text.toString().trim{ it <= ' '}) -> {
                showErrorSnackBar(resources.getString(R.string.err_msg_enter_reppassword),true)
                false
            }

            inputPassword?.editText?.text.toString().trim {it <= ' '} != inputRepPass?.editText?.text.toString().trim{it <= ' '} -> {
                showErrorSnackBar(resources.getString(R.string.err_msg_password_mismatch),true)
                false
            }


            else -> {
                //showErrorSnackBar("Your details are valid",false)
                true
            }
        }


    }

    fun backToLogin(view: View) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}