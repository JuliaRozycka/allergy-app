package edu.ib.zpo2project.account

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.marginTop
import androidx.core.widget.ContentLoadingProgressBar
import com.google.android.gms.tasks.Task
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import edu.ib.zpo2project.HomeActivity
import edu.ib.zpo2project.R

class LoginActivity : BaseActivity() {

    private var inputEmail: TextInputLayout? = null
    private var inputPassword: TextInputLayout? = null
    private var loginButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        inputEmail = findViewById(R.id.loginEmail)
        inputPassword = findViewById(R.id.loginPassword)
        loginButton = findViewById(R.id.loginbtn)

        loginButton?.setOnClickListener {

            logInRegisterUser()

        }

    }

    private fun validateLoginDetails(): Boolean {

        return when {
            TextUtils.isEmpty(inputEmail?.editText?.text.toString().trim { it <= ' ' }) -> {
                showErrorSnackBar(resources.getString(R.string.err_msg_enter_email), true)
                false
            }

            TextUtils.isEmpty(inputPassword?.editText?.text.toString().trim { it <= ' ' }) -> {
                showErrorSnackBar(resources.getString(R.string.err_msg_enter_password), true)
                false
            }

            else -> {
                showErrorSnackBar("Your details are valid", false)
                true
            }
        }
    }

    private fun logInRegisterUser() {
        if (validateLoginDetails()) {
            val email = inputEmail?.editText?.text.toString().trim() { it <= ' ' }
            val password = inputPassword?.editText?.text.toString().trim() { it <= ' ' }


            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->

                    if (task.isSuccessful) {
                        showErrorSnackBar("You are logged in successfully.", false)
                        goToMainActivity()
                        finish()

                    } else {
                        showErrorSnackBar(task.exception!!.message.toString(), true)
                    }
                }
        }
    }


    open fun goToMainActivity() {

        val user = FirebaseAuth.getInstance().currentUser;
        val uid = user?.email.toString()

        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra("uID", uid)
        startActivity(intent)
    }

    fun registerClick(view: View) {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    fun forgetPasswordClick(view: View) {
        //TODO: not implemented yet if user has forgotten password
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Recover password")

        val layout = LinearLayout(this)
        val email = EditText(this)
        email.hint = "Enter email"
        layout.setPadding(20,20,20,20)
        layout.orientation = LinearLayout.VERTICAL
        layout.addView(email)
        builder.setView(layout)

        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        builder.setPositiveButton("Send recover email", DialogInterface.OnClickListener {
                dialog, which ->  val emailStr: String = email.text.toString().trim(); beginRecover(emailStr)
        })
        builder.create()?.show()
    }

    private fun beginRecover(email: String) {
        FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if(task.isSuccessful){
                Toast.makeText(this@LoginActivity, "Email sent", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this@LoginActivity,"Error Occurred",Toast.LENGTH_LONG).show();
            }
        }.addOnFailureListener{e -> Toast.makeText(this@LoginActivity, "Error. Failed.", Toast.LENGTH_SHORT).show()}
    }

}