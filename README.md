# General information

It is a mobile application created in Android Studio (Java/Kotlin) for monitoring air quality and pollen. Firebase was also used to enable register, login and storage user data. 

# Views

After logging in user has access to 3 main fragments: statistics, map and journal. Also there is a side bar menu that consists of settings, user data and general information about the app.

![]()
<img src="airnow1.jpg"  width="250">


![]()
<img src="airnow2.jpg"  width="250">

App also sends daily notifications on pollen and pollution.

![]()
<img src="powiadomieniaairnow.jpg"  width="250">

